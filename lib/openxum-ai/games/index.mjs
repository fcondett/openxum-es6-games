import Abande from './abande/index.mjs';
import Dakapo from './dakapo/index.mjs';
import Dvonn from './dvonn/index.mjs';
import Invers from './invers/index.mjs';
import Kikotsoka from './kikotsoka/index.mjs';
import Neutreeko from "./neutreeko/index.mjs";
import Lines_of_action from "./lines_of_action/index.mjs";

export default {
  Abande: Abande,
  Dakapo: Dakapo,
  Dvonn: Dvonn,
  Invers: Invers,
  Kikotsoka: Kikotsoka,
  Neutreeko: Neutreeko,
    Lines_of_action: Lines_of_action
};