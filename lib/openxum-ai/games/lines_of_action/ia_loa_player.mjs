"use strict";

import AlphaBetaPlayer from '../../../openxum-ai/generic/alphabeta_player.mjs';
import Lines_of_action from '../../../openxum-core/games/lines_of_action/index.mjs';

class IALinesOfActionPlayer extends AlphaBetaPlayer {
    constructor(c, o, e) {
        super(c, o, e, 1, 1000);
    }
    evaluate(e, depth){
        let score =0;
        let pawns = e.get_pawns_of_current_color();
        let count_pawn = e.get_count_pawn_of_current_color();
        for(let i = 0; i < count_pawn; i++){
            score += this._get_neighbors(pawns[i].get_coordinates()) * 50;
        }
        return score;
    }

    _get_neighbors(coord){
        let neighbors = 0;
        let current_x;
        let current_y;

        for(let i = -1; i <= 1; i++) {
           for (let j = -1; j <= 1; j++) {
               current_x = coord._x + i;
               current_y = coord._y + j;

               if(current_x >= 0 && current_x < Lines_of_action.Dimension.DEFAULT_WIDTH &&
                   current_y >= 0 && current_y < Lines_of_action.Dimension.DEFAULT_HEIGHT){

                   let neighbor_pawn = this._engine.get_cell_at(current_x, current_y);

                   if(neighbor_pawn.get_color() === this._color &&
                        (i !== 0 || j !== 0)) {
                         neighbors++;
                   }
                   }
               }
           }
           return neighbors;
    }
}

export default IALinesOfActionPlayer;