"use strict";
import Graphics from '../../graphics/index.mjs';
import OpenXum from '../../openxum/gui.mjs';
import lines_of_action from "../../../openxum-core/games/lines_of_action/index.mjs";

class Gui extends OpenXum.Gui {
    constructor(c, e, l, g) {
        super(c, e, l, g);
        this._deltaX = 0;
        this._deltaY = 0;
        this._offsetX = 0;
        this._offsetY = 0;
        this._is_animating = false;
        this._selected_pawn = undefined;
    }

    draw() {
        this._context.lineWidth = 0.01;
        this._context.fillStyle = "#ffffff";
        Graphics.board.draw_round_rect(this._context, 0, 0, this._canvas.width, this._canvas.height, 17, true, true);
        this._draw_grid();
        this._draw_labels();
        this._draw_state();
    }

    get_move() {
            return this._move;
    }

    get_moves() {
        return this._move.to_string();
    }

    is_animate() {
        return false;
    }

    is_remote() {
        return false;
    }

    unselect() {
        this._selected_pawn = undefined;
    }

    move(move, color) {
        this._is_animating = false;
        this._manager.play();
    }

    set_canvas(c) {
        super.set_canvas(c);
        this._canvas.addEventListener("click", (e) => {
            this._on_click(e);
        });
        this._deltaX = (this._canvas.width * 0.88) / this._engine._board.getWidth();
        this._deltaY = (this._canvas.height * 0.88) / this._engine._board.getHeight();
        this._offsetX = (this._canvas.width - this._deltaX * this._engine._board.getWidth()) / 2;
        this._offsetY = (this._canvas.height - this._deltaY * this._engine._board.getHeight()) / 2;

        this._context.font = "30px Bookman Old Style";
        this.draw();
    }

    _draw_grid() {
        let i, j;
        let alt = 1;
        for (i = 0; i < 8; ++i) {
            for (j = 0; j < 8; ++j) {
                this._context.beginPath();
                this._context.moveTo(this._offsetX + i * this._deltaX, this._offsetY + j * this._deltaY);
                this._context.lineTo(this._offsetX + (i + 1) * this._deltaX - 2, this._offsetY + j * this._deltaY);
                this._context.lineTo(this._offsetX + (i + 1) * this._deltaX - 2, this._offsetY + (j + 1) * this._deltaY - 2);
                this._context.lineTo(this._offsetX + i * this._deltaX, this._offsetY + (j + 1) * this._deltaY - 2);
                this._context.moveTo(this._offsetX + i * this._deltaX, this._offsetY + j * this._deltaY);
                this._context.closePath();
                if (alt === 0) {
                    alt = 1;
                    this._context.fillStyle = "#D59D6C";
                    this._context.fill();
                }
                else if (alt === 1) {
                    alt = 0;
                    this._context.fillStyle = "#ffd8a9";
                    this._context.fill();
                }
            }
            if (alt === 0) alt = 1;
            else alt = 0;
        }
    }

    _draw_state() {
        let i;
        let j;
        let x;
        let y;
        let color;
        for ( i = 0; i < 8; i++) {
            for ( j = 0; j < 8; j++) {
                if (this._selected_pawn !== undefined ){
                    this._draw_selected_pawn();
                }
                if (this._engine.get_cell_at(i, j)._color !== -1) {
                    x = this._engine.get_cell_at(i, j)._coordinates._x;
                    y = this._engine.get_cell_at(i, j)._coordinates._y;
                    color = this._engine.get_cell_at(i, j)._color;
                    this._draw_pawn(x, y, color);
                }
            }
        }
    }

    _draw_pawn(x, y, color) {

        switch (color) {
            case 1 :
                this._context.strokeStyle = "#000000";
                this._context.fillStyle = "#000000";
                break;
            case 2 :
                this._context.strokeStyle = "#ffffff";
                this._context.fillStyle = "#ffffff";
                break;
        }
        let radius = this._deltaX * 0.4;
        let pt = this._compute_coordinates(x, y);
        this._context.lineWidth = 1;
        this._context.beginPath();
        this._context.arc(pt[0], pt[1], radius, 0.0, 2 * Math.PI);
        this._context.closePath();
        this._context.fill();
        this._context.stroke();
    }

    _draw_labels() {
        let i;
        let j = 9 ;
        this._context.lineWidth = 1;
        this._context.fillStyle = "#000000";
        for (i = 0; i < lines_of_action.Dimension.DEFAULT_HEIGHT; ++i) {
            this._context.beginPath();
            this._context.moveTo(this._offsetX - 25, this._offsetY + (i + 0.3) * this._deltaY);
            this._context.fillText('' + (j - 1), this._offsetX - 25 , this._offsetY + (i + 0.6) * this._deltaY);
            this._context.fillText('' + (j - 1), this._offsetX + 510 , this._offsetY + (i + 0.6) * this._deltaY);
            this._context.closePath();
            this._context.fill();
            j--;
        }
        for (i = 0; i < lines_of_action.Dimension.DEFAULT_WIDTH; ++i) {
            this._context.beginPath();
            this._context.moveTo(this._offsetX + this._deltaX * (i + 0.3), this._offsetY - 25);
            this._context.fillText(String.fromCharCode('A'.charCodeAt(0) + i), this._offsetX + this._deltaX * (i + 0.4), this._offsetY - 5);
            this._context.fillText(String.fromCharCode('A'.charCodeAt(0) + i), this._offsetX + this._deltaX * (i + 0.4), 595 - this._offsetY );
            this._context.closePath();
            this._context.fill();
        }
    }

    _compute_coordinates(x, y) {
        return [this._offsetX + x * this._deltaX + (this._deltaX / 2) - 1, this._offsetY + y * this._deltaY + (this._deltaY / 2) - 1];
    }

    _get_click_position(e) {
        let rect = this._canvas.getBoundingClientRect();
        let x = (e.clientX - rect.left) * this._scaleX - this._offsetX;
        let y = (e.clientY - rect.top) * this._scaleY - this._offsetY;
        return {x: Math.floor(x / this._deltaX), y: Math.floor(y / this._deltaX)};
    }

    _on_click(event) {
        let rect = this._canvas.getBoundingClientRect();
        let x = (event.clientX - rect.left) * this._scaleX - this._offsetX;
        let y = (event.clientY - rect.top) * this._scaleY - this._offsetY;
        let pos = {x: Math.floor(x / this._deltaX), y: Math.floor(y / this._deltaX)};
        this._move_list = [];
        if (!this._engine.is_finished()) {
            if (pos.x >= 0 && pos.x < this._engine._board.getWidth() && pos.y >= 0 && pos.y < this._engine._board.getHeight()) {
                if (this._engine.get_cell_at(pos.x, pos.y)._color !== -1
                    && this._engine.get_cell_at(pos.x, pos.y)._color === this._engine.current_color()) {
                    this._selected_pawn = this._engine.get_cell_at(pos.x, pos.y);
                    this._draw_selected_pawn();
                }
                let possible_moves = this._engine.get_possible_moves_of(this._selected_pawn);
                if((this._selected_pawn !== undefined) && (this._engine.get_cell_at(pos.x, pos.y)._color !== this._selected_pawn._color)){
                    for (let i = 0; i < possible_moves.length; i++) {
                        if((pos.x === possible_moves[i].x()) && (pos.y === possible_moves[i].y()) ) {
                            this._move = this._engine.build_move(this._engine.current_color(), this._selected_pawn._coordinates, possible_moves[i]);
                            this._manager.play();
                        }
                    }
                    this.unselect();
                }
            }
            this.draw();
        }
    }

    _draw_selected_pawn() {
        let x = this._selected_pawn._coordinates._x ;
        let y = this._selected_pawn._coordinates._y ;
        let pt = this._compute_coordinates(x, y);
        let possible_moves = this._engine.get_possible_moves_of(this._selected_pawn);
        this._draw_pawn(x, y, this._selected_pawn._color);
        let radius = (this._deltaX / 2.3);
        this._context.lineWidth = 4;
        this._context.strokeStyle = "#d8370f";
        this._context.beginPath();
        this._context.arc(pt[0], pt[1], radius, 0.0, 2 * Math.PI);
        this._context.closePath();
        this._context.stroke();
        for (let i = 0; i < possible_moves.length; i++) {
            let xx = possible_moves[i].x();
            let yy = possible_moves[i].y();
            let pt = this._compute_coordinates(xx, yy);
            this._context.fillStyle = "#d8370f";
            radius = (this._deltaX / 15);
            this._context.beginPath();
            this._context.arc(pt[0], pt[1],radius, 0.0, 2 * Math.PI);
            this._context.closePath();
            this._context.fill();
        }
    }
}

export default Gui;