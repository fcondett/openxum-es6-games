"use strict";

import lines_of_action from '../../../openxum-core/games/lines_of_action/index.mjs';
import AI from '../../../openxum-ai/generic/index.mjs';
import Gui from './gui.mjs';
import Manager from './manager.mjs';
import IALinesOfActionPlayer from "../../../openxum-ai/games/lines_of_action/ia_loa_player.mjs";

export default {
    Gui: Gui,
    Manager: Manager,
    Settings: {
        ai: {
            mcts: IALinesOfActionPlayer  // TODO: MCTSPlayer
        },

        colors: {
            first: lines_of_action.Color.BLACK,
            init: lines_of_action.Color.BLACK,
            list: [
                {key: lines_of_action.Color.BLACK, value: 'Black'},
                {key: lines_of_action.Color.WHITE, value: 'White'}
            ]
        },
        modes: {
            init: lines_of_action.GameType.STANDARD,
            list: [
                {key: lines_of_action.GameType.STANDARD, value: 'standard'},
            ]
        },
        opponent_color(color) {
            return color === lines_of_action.Color.BLACK ? lines_of_action.Color.WHITE : lines_of_action.Color.BLACK;
        },
        types: {
            init: 'ai',
            list: [
                {key: 'gui', value: 'GUI'},
                {key: 'ai', value: 'AI'},
                {key: 'online', value: 'Online'},
                {key: 'offline', value: 'Offline'}
            ]
        }
    }
};
