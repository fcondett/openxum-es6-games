"use strict";

import OpenXum from '../../openxum/manager.mjs';
import Color from '../../../openxum-core/games/lines_of_action/color.mjs';
import lines_of_action from "../../../openxum-core/games/lines_of_action/index.mjs";

class Manager extends OpenXum.Manager {
    constructor(t, e, g, o, s, w, f) {
        super(t, e, g, o, s, w, f);
        this.that(this);
    }

    build_move() {
        return new lines_of_action.Move();
    }

    get_current_color() {
        switch (this._engine.current_color()) {
            case Color.WHITE:
                return 'White';
            case Color.BLACK:
                return 'Black';
            default:
                return 'Nobody';
        }    }

    get_name() {
        return 'lines_of_action';
    }

    get_winner_color() {
        return this._engine.winner_is() === 2 ? 'White' : 'Black';
    }

    process_move() {
    }
}

export default Manager;
