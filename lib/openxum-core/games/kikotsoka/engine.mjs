"use strict";

import Color from './color.mjs';
import Coordinates from './coordinates.mjs';
import Move from './move.mjs';
import MoveType from './move_type.mjs';
import OpenXum from '../../openxum/index.mjs';
import Patterns from './patterns.mjs';
import Phase from './phase.mjs';
import State from './state.mjs';

const config = [
  {
    size: 11,
    piece_number: 32
  },
  {
    size: 14,
    piece_number: 42
  },
  {
    size: 17,
    piece_number: 52
  }
];

class Engine extends OpenXum.Engine {
  constructor(t, c) {
    super();
    this._type = t;
    this._color = c;
    this._board = [];
    for (let l = 0; l < config[t].size; ++l) {
      this._board[l] = [];
      for (let c = 0; c < config[t].size; ++c) {
        this._board[l][c] = State.VACANT;
      }
    }
    this._black_piece_number = config[t].piece_number;
    this._white_piece_number = config[t].piece_number;
    this._black_shido_number = 5;
    this._white_shido_number = 5;
    this._last_coordinates = null;
    this._pass = 0;
    this._black_captured_piece_number = 0;
    this._white_captured_piece_number = 0;
    this._black_level = 0;
    this._white_level = 0;
    this._black_failed = false;
    this._white_failed = false;
    this._size = config[t].size;
    this._phase = Phase.PUT_SHIDO;
  }

// public methods
  build_move() {
    return new Move();
  }

  clone() {
    let o = new Engine(this._type, this._color);

    for (let l = 0; l < this._size; ++l) {
      for (let c = 0; c < this._size; ++c) {
        o._board[l][c] = this._board[l][c];
      }
    }
    o._black_piece_number = this._black_piece_number;
    o._white_piece_number = this._white_piece_number;
    o._black_shido_number = this._black_shido_number;
    o._white_shido_number = this._white_shido_number;
    o._last_coordinates = this._last_coordinates;
    o._pass = this._pass;
    o._black_captured_piece_number = this._black_captured_piece_number;
    o._white_captured_piece_number = this._white_captured_piece_number;
    o._black_level = this._black_level;
    o._white_level = this._white_level;
    o._black_failed = this._black_failed;
    o._white_failed = this._white_failed;
    o._size = this._size;
    o._phase = this._phase;
    return o;
  }

  current_color() {
    return this._color;
  }

  get_name() {
    return 'Kikotsoka';
  }

  get_possible_move_list() {
    let list = [];

    if (this._phase === Phase.PUT_SHIDO) {
      for (let l = 0; l < this._size; ++l) {
        for (let c = 0; c < this._size; ++c) {
          if (this.is_valid(l, c)) {
            list.push(new Move(MoveType.PUT_SHIDO, this._color, new Coordinates(c, l), -1));
          }
        }
      }
    } else if (this._phase === Phase.PUT_PIECE) {
      if ((this._color === Color.BLACK && this._black_piece_number > 0) ||
        (this._color === Color.WHITE && this._white_piece_number > 0)) {
        const possible_patterns = this._is_possible_patterns();
        const possible_cases_number = this._count_possible_cases(possible_patterns);

        if (possible_cases_number > 0) {
          const one_piece_patterns = this._get_one_piece_pattern(possible_patterns);

          for (let l = 0; l < this._size; ++l) {
            for (let c = 0; c < this._size; ++c) {
              if (this.is_valid(l, c) && this._is_connect(l, c) && !this._possible_forbidden_pattern(l, c, one_piece_patterns)) {
                list.push(new Move(MoveType.PUT_PIECE, this._color, new Coordinates(c, l), -1));
              }
            }
          }
        }
      }
      if (list.length === 0) {
        list.push(new Move(MoveType.PASS, this._color, null, -1));
      }
    } else if (this._phase === Phase.CHOICE_PIECE) {
      list.push(new Move(MoveType.CHOICE_PIECE, this._color, null, 0));
      list.push(new Move(MoveType.CHOICE_PIECE, this._color, null, 1));
    } else { // phase = Phase.CHOICE_PATTERN
      const result = this._check_patterns();

      for (let i = 0; i < result.length; ++i) {
        list.push(new Move(MoveType.CHOICE_PATTERN, this._color, null, i));
      }
    }
    return list;
  }

  get_type() {
    return this._type;
  }

  is_finished() {
    return this._black_level === 5 || this._white_level === 5 || this._pass === 2 || this._black_failed || this._white_failed ||
      (this._black_piece_number === 0 && this._white_piece_number === 0);
  }

  is_valid(l, c) {
    return this._is_valid(l, c) && this._board[l][c] === State.VACANT;
  }

  move(move) {
    if (move !== null) {
      if (move.type() === MoveType.PUT_SHIDO) {
        this._board[move.to().line()][move.to().column()] =
          move.color() === Color.BLACK ? State.BLACK_SHIDO : State.WHITE_SHIDO;
        if (this._color === Color.BLACK) {
          --this._black_shido_number;
        } else {
          --this._white_shido_number;
        }
        this._change_color();
        this._next_phase();
      } else if (move.type() === MoveType.PUT_PIECE) {
        this._board[move.to().line()][move.to().column()] =
          move.color() === Color.BLACK ? State.BLACK : State.WHITE;
        if (this._color === Color.BLACK) {
          --this._black_piece_number;
        } else {
          --this._white_piece_number;
        }
        this._last_coordinates = move.to();

        const result = this._check_patterns();

        if (result.length > 0) {
          if (result.length === 1) {
            this._capture(result[0]);
            this._block(result[0]);
            if (this._color === Color.BLACK) {
              ++this._black_level;
            } else {
              ++this._white_level;
            }
            this._change_color();
            this._next_phase();
          } else {
            this._phase = Phase.CHOICE_PATTERN;
          }
        } else {
          this._change_color();
          this._next_phase();
        }
        this._pass = 0;
      } else if (move.type() === MoveType.CHOICE_PIECE) {
        if (move.index() === 0) {
          this._phase = Phase.PUT_SHIDO;
        } else {
          this._phase = Phase.PUT_PIECE;
        }
      } else if (move.type() === MoveType.CHOICE_PATTERN) {
        const result = this._check_patterns();

        this._capture(result[move.index()]);
        this._block(result[move.index()]);
        if (this._color === Color.BLACK) {
          ++this._black_level;
        } else {
          ++this._white_level;
        }
        this._change_color();
        this._next_phase();
      } else if (move.type() === MoveType.PASS) {
        ++this._pass;
        this._last_coordinates = null;
        this._change_color();
        this._next_phase();
      }
    }
  }

  parse(str) {
    // TODO
  }

  phase() {
    return this._phase;
  }

  to_string() {
    let str = '   ';

    for (let c = 0; c < this._size; ++c) {
      str += ' ' + String.fromCharCode('A'.charCodeAt(0) + c) + ' ';
    }
    str += '\n';
    for (let l = 0; l < this._size; ++l) {
      if (l < 9) {
        str += (l + 1) + '  ';
      } else {
        str += (l + 1) + ' ';
      }
      for (let c = 0; c < this._size; ++c) {
        str += State.to_string(this._board[l][c]);
      }
      str += '\n';
    }
    return str;
  }

  winner_is() {
    if (this.is_finished()) {
      if (this._black_level === 5 || this._black_level > this._white_level || this._white_failed) {
        return Color.BLACK;
      } else if (this._white_level === 5 || this._black_level < this._white_level || this._black_failed) {
        return Color.WHITE;
      } else {
        if (this._black_captured_piece_number > this._white_captured_piece_number) {
          return Color.BLACK;
        } else if (this._black_captured_piece_number < this._white_captured_piece_number) {
          return Color.WHITE;
        } else {
          return Color.NONE;
        }
      }
    } else {
      return false;
    }
  }

// private methods
  _block(origin) {
    let l = origin.l;
    let c = origin.c;

    while (l < origin.l + 3) {
      this._block_coordinates(l, c);
      ++c;
      if (c === origin.c + 3) {
        c = origin.c;
        ++l;
      }
    }
  }

  _block_coordinates(l, c) {
    const old_value = this._board[l][c];
    let new_value = this._color === Color.BLACK ? State.BLOCKED_IN_BLACK : State.BLOCKED_IN_WHITE;

    if (old_value === State.BLACK && this._color === Color.BLACK) {
      new_value = State.BLACK_BLOCKED;
    } else if (old_value === State.WHITE && this._color === Color.WHITE) {
      new_value = State.WHITE_BLOCKED;
    } else if (old_value === State.BLACK_SHIDO && this._color === Color.BLACK) {
      new_value = State.BLACK_SHIDO_BLOCKED;
    } else if (old_value === State.WHITE_SHIDO && this._color === Color.WHITE) {
      new_value = State.WHITE_SHIDO_BLOCKED;
    }
    this._board[l][c] = new_value;
  }

  _capture(origin) {
    let l = origin.l;
    let c = origin.c;
    let n = 0;

    while (l < origin.l + 3) {
      if ((this._board[l][c] === State.BLACK || this._board[l][c] === State.BLACK_SHIDO) && this._color === Color.WHITE) {
        this._board[l][c] = State.VACANT;
        ++n;
      }
      if ((this._board[l][c] === State.WHITE || this._board[l][c] === State.WHITE_SHIDO) && this._color === Color.BLACK) {
        this._board[l][c] = State.VACANT;
        ++n;
      }
      ++c;
      if (c === origin.c + 3) {
        c = origin.c;
        ++l;
      }
    }
    if (this._color === Color.BLACK) {
      this._black_captured_piece_number += n;
      this._black_piece_number += n;
    } else {
      this._white_captured_piece_number += n;
      this._white_piece_number += n;
    }
  }

  _change_color() {
    this._color = this._color === Color.BLACK ? Color.WHITE : Color.BLACK;
  }

  _check_no_blocked(origin) {
    let l = origin.l;
    let c = origin.c;
    let blocked = false;

    while (!blocked && l < origin.l + 3) {
      blocked = (this._board[l][c] === State.BLOCKED_IN_BLACK || this._board[l][c] === State.BLOCKED_IN_WHITE ||
        this._board[l][c] === State.BLACK_BLOCKED || this._board[l][c] === State.WHITE_BLOCKED);
      ++c;
      if (c === origin.c + 3) {
        c = origin.c;
        ++l;
      }
    }
    return !blocked;
  }

  _check_pattern(pattern) {
    let l = 0;
    let c = 0;
    let origins = [];

    while (l < this._size - 2) {
      if (this._check_no_blocked({l: l, c: c})) {
        if (this._check_pattern_in_block({l: l, c: c}, pattern)) {
          origins.push({l: l, c: c});
        }
      }
      ++c;
      if (c === this._size - 2) {
        c = 0;
        ++l;
      }
    }
    return origins;
  }

  _check_pattern_in_block(origin, pattern) {
    let l = origin.l;
    let c = origin.c;
    let ok = true;

    while (ok && l < origin.l + 3) {
      const value = pattern[l - origin.l][c - origin.c];

      if ((value === 0 &&
        this._board[l][c] !== (this._color === Color.BLACK ? State.BLACK : State.WHITE) &&
        this._board[l][c] !== (this._color === Color.BLACK ? State.BLACK_SHIDO : State.WHITE_SHIDO)) ||
        (value === 1 &&
          (this._board[l][c] === (this._color === Color.BLACK ? State.BLACK : State.WHITE) ||
            this._board[l][c] === (this._color === Color.BLACK ? State.BLACK_SHIDO : State.WHITE_SHIDO)))) {
        ++c;
        if (c === origin.c + 3) {
          c = origin.c;
          ++l;
        }
      } else {
        ok = false;
      }
    }
    return ok;
  }

  _check_patterns() {
    let level = 0;
    let found = false;
    let origins = [];

    while (!found && level < 5) {
      const pattern = Patterns[level];

      for (let j = 0; j < pattern.length; ++j) {
        const new_origins = this._check_pattern(pattern[j]);

        if (new_origins.length > 0) {
          origins = origins.concat(new_origins);
          found = true;
        }
      }
      if (!found) {
        ++level;
      }
    }
    if (found) {
      const current_level = this._color === Color.BLACK ? this._black_level : this._white_level;

      if (level === current_level) {
        return origins;
      } else {
        if (this._color === Color.BLACK) {
          this._black_failed = true;
        } else {
          this._white_failed = true;
        }
        return [];
      }
    } else {
      return [];
    }
  }

  _count_possible_cases(list) {
    const current_level = this._color === Color.BLACK ? this._black_level : this._white_level;
    let counter = 0;

    for (let i = 0; i < Patterns[current_level].length; ++i) {
      counter += list[current_level][i].length;
    }
    return counter;
  }

  _distance(l, c) {
    return Math.abs(this._last_coordinates.line() - l) + Math.abs(this._last_coordinates.column() - c);
  }

  _get_one_piece_pattern(list) {
    let new_list = [];

    for (let level = 0; level < 5; ++level) {
      new_list.push([]);
      for (let i = 0; i < Patterns[level].length; ++i) {
        for (let index = 0; index < list[level][i].length; ++index) {
          if (list[level][i][index].list.length === 1) {
            new_list[level].push(list[level][i][index]);
          }
        }
      }
    }
    return new_list;
  }

  _is_connect(l, c) {
    const color = this._color === Color.BLACK ? State.BLACK : State.WHITE;
    const blocked_color = this._color === Color.BLACK ? State.BLACK_BLOCKED : State.WHITE_BLOCKED;
    const blocked_in_color = this._color === Color.BLACK ? State.BLOCKED_IN_BLACK : State.BLOCKED_IN_WHITE;
    const shido_color = this._color === Color.BLACK ? State.BLACK_SHIDO : State.WHITE_SHIDO;
    const shido_blocked_color = this._color === Color.BLACK ? State.BLACK_SHIDO_BLOCKED : State.WHITE_SHIDO_BLOCKED;
    let list = [];
    let found = false;
    let new_list = [{l: l, c: c}];

    while (!found && new_list.length > 0) {
      const current = new_list.pop();

      list.push(current);
      for (let i = -1; i < 2; ++i) {
        for (let j = -1; j < 2; ++j) {
          if (i !== 0 || j !== 0) {
            const new_element = {l: current.l + i, c: current.c + j};

            if (new_element.l >= 0 && new_element.l < this._size && new_element.c >= 0 && new_element.c < this._size) {
              if (list.findIndex((e) => {
                return e.l === new_element.l && e.c === new_element.c
              }) === -1) {
                if (this._board[new_element.l][new_element.c] === color ||
                  this._board[new_element.l][new_element.c] === blocked_color) {
                  new_list.push(new_element);
                } else if (this._board[new_element.l][new_element.c] === shido_color ||
                  this._board[new_element.l][new_element.c] === shido_blocked_color ||
                  this._board[new_element.l][new_element.c] === blocked_in_color) {
                  new_list.push(new_element);
                  found = true;
                }
              }
            }
          }
        }
      }
    }
    return found;
  }

  _is_possible_pattern(pattern) {
    let l = 0;
    let c = 0;
    let result = [];

    while (!result.ok && l < this._size - 2) { // remove "!result.ok"
      const result_in_block = this._is_possible_pattern_in_block({l: l, c: c}, pattern);

      if (result_in_block.ok) {
        result.push(result_in_block);
      }
      ++c;
      if (c === this._size - 2) {
        c = 0;
        ++l;
      }
    }
    return result;
  }

  _is_possible_pattern_in_block(origin, pattern) {
    let l = origin.l;
    let c = origin.c;
    let result = {ok: true, list: []};
    const color_state = this._color === Color.BLACK ? State.BLACK : State.WHITE;
    const shido_color_state = this._color === Color.BLACK ? State.BLACK_SHIDO : State.WHITE_SHIDO;

    while (result.ok && l < origin.l + 3) {
      const value = pattern[l - origin.l][c - origin.c];
      const state = this._board[l][c];

      if (state !== State.BLOCKED_IN_BLACK && state !== State.BLOCKED_IN_WHITE && state !== State.BLACK_BLOCKED && state !== State.WHITE_BLOCKED &&
        (value === 0 && state !== color_state && state !== shido_color_state) ||
        (value === 1 && (state === color_state || state === shido_color_state || state === State.VACANT))) {
        if (value === 1 && state === State.VACANT) {
          result.list.push({line: l, column: c});
        }
        ++c;
        if (c === origin.c + 3) {
          c = origin.c;
          ++l;
        }
      } else {
        result.ok = false;
      }
    }
    return result;
  }

  _is_possible_patterns() {
    let result = [];

    for (let level = 0; level < 5; ++level) {
      const pattern = Patterns[level];

      result.push([]);
      for (let j = 0; j < pattern.length; ++j) {
        const result_pattern = this._is_possible_pattern(pattern[j]);

        result[level].push(result_pattern);
      }
    }
    return result;
  }

  _is_valid(l, c) {
    const opponent_level = this._color === Color.WHITE ? this._black_level : this._white_level;
    const distance = opponent_level === 0 ? 0 : opponent_level === 1 || opponent_level === 2 ? 1 : 2;

    return this._last_coordinates === null || (this._last_coordinates !== null && this._distance(l, c) > distance);
  }

  _next_phase() {
    if (this._color === Color.BLACK) {
      if (this._black_shido_number === 0) {
        this._phase = Phase.PUT_PIECE;
      } else if (this._black_shido_number === 5) {
        this._phase = Phase.PUT_SHIDO;
      } else {
        this._phase = Phase.CHOICE_PIECE;
      }
    } else {
      if (this._white_shido_number === 0) {
        this._phase = Phase.PUT_PIECE;
      } else if (this._white_shido_number === 5) {
        this._phase = Phase.PUT_SHIDO;
      } else {
        this._phase = Phase.CHOICE_PIECE;
      }
    }
  }

  _possible_forbidden_pattern(l, c, list) {
    const current_level = this._color === Color.BLACK ? this._black_level : this._white_level;
    let ok = true;
    let level = 0;

    while (ok && level < 5) {
      if (level !== current_level) {
        let index = 0;

        while (ok && index < list[level].length) {
          if (list[level][index].list[0].column === c && list[level][index].list[0].line === l) {
            ok = false;
          } else {
            ++index;
          }
        }
      }
      if (ok) {
        ++level;
      }
    }
    return !ok;
  }
}

export default Engine;