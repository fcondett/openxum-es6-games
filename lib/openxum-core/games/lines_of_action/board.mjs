"use strict";

import Color from './color.mjs';
import Pawn from './pawn.mjs';
import Coordinates from './coordinates.mjs';
import Dimension from './dimension.mjs';
import PawnLists from "./pawn_list.mjs";
import ConnectedPawns from './connected_pawns.mjs';

class Board {
    constructor(pl){
        this._pawnList = new PawnLists(pl);
        this._board = new Array(Dimension.DEFAULT_WIDTH);
        this._width =Dimension.DEFAULT_WIDTH;
        this._height = Dimension.DEFAULT_HEIGHT;

        for (let i = 0; i < Dimension.DEFAULT_WIDTH; i++) {
            this._board[i] = new Array(Dimension.DEFAULT_HEIGHT);
            for(let j = 0; j < Dimension.DEFAULT_HEIGHT; j++){
                let current_coordinates  = new Coordinates(i,j);
                let current_color = this._pawnList.find_pawn(current_coordinates);
                this._board[i][j] = new Pawn(current_color, current_coordinates);
            }
        }
    }

    getHeight() {
        return Dimension.DEFAULT_HEIGHT;
    }

    getWidth() {
        return Dimension.DEFAULT_WIDTH;
    }

    get_pawn_at(i,j) {
        console.assert(
            i>=0 && i<this._width && j>=0 && j<this._height,
            'Invalide coordinates: ' + i + ',' + j
        );
        return this._board[i][j];
    }

    get_pawns_of(color){
        return this._pawnList.get_pawns_of(color);
    }

    get_count_pawn_of(color){
        let count_pawn = (color === Color.BLACK) ? this._pawnList.count_black_pawn() : this._pawnList.count_white_pawn();

        return count_pawn;
    }

    move_pawn(move){
        let color_move_from = move._color;
        let color_move_to = this._pawnList.find_pawn(move._to);

        if(color_move_to === Color.NONE){
            this._pawnList.change_coord_pawn(move._from, move._to);
        }

        if(color_move_from === Color.WHITE && color_move_to === Color.BLACK){
            this._pawnList.delete_pawn(move._to);
            this._pawnList.change_coord_pawn(move._from, move._to);
        }

        if(color_move_from === Color.BLACK && color_move_to === Color.WHITE){
            this._pawnList.delete_pawn(move._to);
            this._pawnList.change_coord_pawn(move._from, move._to)
        }
        if (move._from != undefined && move._to != undefined) {
            this._board[move._from.x()][move._from.y()].set_color(Color.NONE);
            this._board[move._to.x()][move._to.y()].set_color(move._color);
        }
    }

    delete_pawn(coord)
    {
        this._pawnList.delete_pawn(coord);
        this._board[coord.x()][coord.y()].set_color(Color.NONE);
    }

    get_possible_moves_of_player(color_player){
        return this._pawnList.get_possible_moves_list_player(color_player);
    }

    get_possible_moves_of(pawn){
        return this._pawnList.get_moves_of_current_pawn(pawn);
    }

    _pick_a_pawn_of(color){
        for(let i = 0; i < Dimension.DEFAULT_WIDTH; i++){
            for(let j = 0; j < Dimension.DEFAULT_HEIGHT; j++){
                if(this.get_pawn_at(i,j).get_color() === color){
                    return this._board[i][j];
                }
            }
        }
    }

    is_winning_pawn(last_pawn_moved){
        let count_of_pawn;
        let connected_pawn;

        if (last_pawn_moved.get_color() === Color.BLACK) {
            count_of_pawn = this._pawnList.count_black_pawn();
        }
        else if (last_pawn_moved.get_color() === Color.WHITE) {
            count_of_pawn = this._pawnList.count_white_pawn();
        }
        if(count_of_pawn === 1) {return true;}

        connected_pawn = new ConnectedPawns(last_pawn_moved, count_of_pawn, this._board);

        return connected_pawn.all_pawns_connected();
    }
}

export default Board;