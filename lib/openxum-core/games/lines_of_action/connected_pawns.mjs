"use strict";

import Dimension from './dimension.mjs';

class ConnectedPawns{
    constructor(last_pawn_moved, count_of_pawn, board){
        this._neighbors = [];
        this._visited = [];
        this._wait = [];

        this._board = board;
        this._lpm = last_pawn_moved;
        this._pawns_color = last_pawn_moved.get_color();
        this._count_of_pawn = count_of_pawn;
    }

    _get_neighbors_of(current_pawn){
        this._neighbors = [];
        let current_x;
        let current_y;

        if(current_pawn === 'no pawn to visit' || current_pawn === undefined) {return;}

        for(let i = -1; i <= 1; i++) {
            for (let j = -1; j <= 1; j++) {
                current_x = current_pawn.get_coordinates()._x + i;
                current_y = current_pawn.get_coordinates()._y + j;

                if(current_x >= 0 && current_x < Dimension.DEFAULT_WIDTH &&
                    current_y >= 0 && current_y < Dimension.DEFAULT_HEIGHT){

                    let neighbor_pawn = this._board[current_x][current_y];

                    if(neighbor_pawn.get_color() === this._pawns_color &&
                        this._is_visited(neighbor_pawn) === false &&
                        this._is_waiting(neighbor_pawn) === false &&
                        (i !== 0 || j !== 0)) {
                        this._neighbors.push(this._board[current_x][current_y]);
                    }
                }
            }
        }
    }

    _is_visited(pawn){
        for(let i = 0; i < this._visited.length; i++){
            if(this._visited[i].get_coordinates()._x === pawn.get_coordinates()._x &&
                this._visited[i].get_coordinates()._y === pawn.get_coordinates()._y &&
                this._visited[i].get_color() === pawn.get_color()){
                    return true;
            }
        }
        return false;
    }

    _is_waiting(pawn){
        for(let i = 0; i < this._wait.length; i++){
            if(this._wait[i].get_coordinates()._x === pawn.get_coordinates()._x &&
                this._wait[i].get_coordinates()._y === pawn.get_coordinates()._y &&
                this._wait[i].get_color() === pawn.get_color()){
                return true;
            }
        }
        return false;
    }

    _add_wait(){

        if(this._neighbors === []){return;}

        for(let i = 0; i < this._neighbors.length; i++){
            this._wait.push(this._neighbors[i]);
        }
    }

    _wait_is_empty(){
        return (this._wait.length === 0);
    }

    _pop_wait(){
        if(this._wait.length === 0) {return 'no pawn to visit';}

        return this._wait.pop();
    }

    all_pawns_connected(){
        let current_pawn = this._lpm;
        this._get_neighbors_of(current_pawn);
        this._visited.push(current_pawn);
        this._add_wait();

        while(this._wait_is_empty() === false) {
            current_pawn = this._pop_wait();
            this._get_neighbors_of(current_pawn);
            this._add_wait();
            if (current_pawn !== 'no pawn to visit') {
                this._visited.push(current_pawn);
            }
        }
        return (this._visited.length === this._count_of_pawn);
    }
}

export default ConnectedPawns;