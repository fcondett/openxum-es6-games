"use strict";

const Dimension = {DEFAULT_WIDTH: 8, DEFAULT_HEIGHT: 8};

export default Dimension;