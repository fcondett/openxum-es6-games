"use strict";

import OpenXum from '../../openxum/index.mjs';
import Move from './move.mjs';
import Board from './board.mjs';
import Color from './color.mjs';

class Engine extends OpenXum.Engine {
    constructor(t,c,pl) {
        super();
        this._type = t;
        this._current_color = c;
        this._board = new Board(pl);
    }

    build_move(color, from, to) {
        return new Move(color, from, to );
    }

    clone() {
        return new Engine(this._type, this._current_color, this._board._pawnList);
    }

    get_cell_at(i,j){
        return this._board.get_pawn_at(i,j);
    }

    current_color() {
        return this._current_color;
    }

    get_name() {
        return 'Lines_of_action';
    }

    get_possible_move_list() {
        return this._board.get_possible_moves_of_player(this.current_color());
    }

    get_possible_moves_of(pawn){
        return this._board.get_possible_moves_of(pawn);
    }

    get_pawns_of_current_color(){
        return this._board.get_pawns_of(this._current_color);
    }

    get_count_pawn_of_current_color(){
        return this._board.get_count_pawn_of(this._current_color);
    }

    is_finished() {
        let last_pawn_moved = this._board._pick_a_pawn_of(this._current_color);
        return this._board.is_winning_pawn(last_pawn_moved);
    }

    move(move) {
        this._board.move_pawn(move);
        if (! this.is_finished()){
        this._change_color();}
    }

    parse(str) {
        // TODO
    }

    to_string() {
        // TODO
    }

    winner_is() {
        if(this.is_finished()){return this._current_color;}
        else{ return Color.NONE;}

    }

    _change_color() {
        if(this.current_color() === Color.BLACK)
            this._current_color = Color.WHITE;
        else
            this._current_color = Color.BLACK;
    }
}

export default Engine;
