"use strict";
import Engine from './engine.mjs';
import GameType from './game_type.mjs';
import Move from './move.mjs';
import Color from './color.mjs';
import PawnLists from './pawn_list.mjs';
import Coordinates from './coordinates.mjs';
import Pawn from './pawn.mjs';
import Dimension from './dimension.mjs';
import ConnectedPawns from './connected_pawns.mjs';
export default {
    Engine: Engine,
    GameType: GameType,
    Move: Move,
    Color: Color,
    Coordinates: Coordinates,
    PawnLists: PawnLists,
    Pawn: Pawn,
    Dimension: Dimension,
    ConnectedPawns: ConnectedPawns
}
