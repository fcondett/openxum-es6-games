"use strict";

import OpenXum from '../../openxum/index.mjs';
import Color from './color.mjs';
import Coordinates from "../lines_of_action/coordinates.mjs";

class Move extends OpenXum.Move {
    constructor(c, from, to) {
        super();
        this._color = c;
        this._from = from;
        this._to = to;
    }
    decode(str) {
        this._from = new Coordinates(parseInt(str.charAt(0)), parseInt(str.charAt(1)));
        this._to = new Coordinates(parseInt(str.charAt(2)), parseInt(str.charAt(3)));
    }

    encode() {
        return this._from.to_string() + this._to.to_string();
    }

    from_object(data) {
        this._from = new Coordinates(data.from.column, data.from.line);
        this._to = new Coordinates(data.to.column, data.to.line);
    }

    to_object() {
        return {
            from: this._from === null ? {column: -1, line: -1} : {column: this._from._column, line: this._from._line},
            to: this._to === null ? {column: -1, line: -1} : {column: this._to._column, line: this._to._line},
        };
    }

    from() {
        return this._from;
    }

    to() {
        return this._to;
    }

    to_string() {
        if(this._color === Color.BLACK)
            return 'black' + ' move from:' + this._from.to_string() + ' to:' + this._to.to_string() + ' ;';
        if(this._color === Color.WHITE)
            return 'white' + ' move from:' + this._from.to_string() + ' to:' + this._to.to_string() + ' ;';
    }
}

export default Move;
