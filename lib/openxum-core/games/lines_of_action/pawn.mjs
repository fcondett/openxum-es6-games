"use strict";

class Pawn{
    constructor(col, coord){
        this._coordinates = coord;
        this._color = col;
    }

    get_coordinates(){
        return this._coordinates;
    }

    set_coordinates(coord){
        this._coordinates = coord;
    }

    get_color(){
        return this._color;
    }

    set_color(color){
        this._color = color;
    }

    to_string() {
        return "[color: " + this._color + " coord: " + this._coordinates.to_string() + "]";
    }
}

export default Pawn;