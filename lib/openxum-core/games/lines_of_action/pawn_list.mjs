"use strict";

import Pawn from './pawn.mjs';
import Color from './color.mjs';
import Coordinates from './coordinates.mjs';
import Dimension from './dimension.mjs';
import Move from './move.mjs';

class PawnLists {
    constructor(copy_pawnList) {

        if (copy_pawnList === undefined) {
            this._list = [];
            this._count_black_pawn = 12;
            this._count_white_pawn = 12;
            this._nb_pawn = 24;

            this.initialise_list();
        }
        else {
            this._list = [];
            this._count_black_pawn = copy_pawnList.count_black_pawn();
            this._count_white_pawn = copy_pawnList.count_white_pawn();
            this._nb_pawn = copy_pawnList.nb_pawn();

            for (let nb_pawn = 0; nb_pawn < copy_pawnList.nb_pawn(); nb_pawn++) {
                let pawn = copy_pawnList._list[nb_pawn];
                this._list.push(new Pawn(pawn.get_color(), new Coordinates(pawn.get_coordinates().x(), pawn.get_coordinates().y())));
            }
        }
    }

    initialise_list() {

        for (let count_top_pawn = 0; count_top_pawn < 6; count_top_pawn++) {
            this._list.push(new Pawn(Color.BLACK, new Coordinates(0, count_top_pawn + 1)));
        }

        for (let count_bottom_pawn = 0; count_bottom_pawn < 6; count_bottom_pawn++) {
            this._list.push(new Pawn(Color.BLACK, new Coordinates(Dimension.DEFAULT_WIDTH - 1, count_bottom_pawn + 1)));
        }

        for (let count_left_pawn = 0; count_left_pawn < 6; count_left_pawn++) {
            this._list.push(new Pawn(Color.WHITE, new Coordinates(count_left_pawn + 1, 0)));
        }

        for (let count_right_pawn = 0; count_right_pawn < 6; count_right_pawn++) {
            this._list.push(new Pawn(Color.WHITE, new Coordinates(count_right_pawn + 1, Dimension.DEFAULT_HEIGHT - 1)));
        }
    }

    count_black_pawn() {
        return this._count_black_pawn;
    }

    count_white_pawn() {
        return this._count_white_pawn;
    }

    nb_pawn() {
        return this._nb_pawn;
    }

    get_pawn(index) {
        return this._list[index];
    }

    get_pawns_of(color){
        let pawns = [];

        for(let i = 0; i < this._nb_pawn; i++){
            if(this.get_pawn(i).get_color() === color){
                pawns.push(this.get_pawn(i));
            }
        }

        return pawns;
    }

    find_pawn(coord) {
        if (coord != undefined) {
            for (let i = 0; i < this.nb_pawn(); i++) {
                let current_pawn = this._list[i];

                if(current_pawn === undefined){return;}

                if (coord._y === current_pawn._coordinates._y && coord._x === current_pawn._coordinates._x) {
                    return current_pawn.get_color();
                }
            }
        }
        return Color.NONE;
    }


    change_coord_pawn(coord1, coord2) {
        if (coord1 != undefined && coord2 != undefined) {
            for (let i = 0; i < this.nb_pawn(); i++) {
                let current_pawn = this.get_pawn(i);
                if (coord1.y() === current_pawn.get_coordinates().y() && coord1.x() === current_pawn.get_coordinates().x()) {
                    this._list[i].set_coordinates(coord2);
                    return;
                }
            }
        }
    }

    delete_pawn(coord) {
        if (coord != undefined) {
            for (let i = 0; i < this.nb_pawn(); i++) {
                let current_pawn_coordinate = this.get_pawn(i).get_coordinates();
                if (coord._y === current_pawn_coordinate.y() && coord._x === current_pawn_coordinate.x()) {
                    this._nb_pawn--;

                    if (this.get_pawn(i).get_color() === Color.BLACK) {
                        this._count_black_pawn--;
                    }
                    if (this.get_pawn(i).get_color() === Color.WHITE) {
                        this._count_white_pawn--;
                    }
                    this._list.splice(i, 1);
                    return;
                }
            }
        }
    }

    get_possible_moves_list_player(color_player){
        let count_of_pawn;
        let count_of_pawn_index = 0;
        let list_new_coord;
        let current_move;
        let moves_list_player = [];

        if (color_player === Color.BLACK) {
            count_of_pawn = this.count_black_pawn();
        }
        else if (color_player === Color.WHITE) {
            count_of_pawn = this.count_white_pawn();
        }

        for(let i = 0; i < this.nb_pawn(); i++){
            if(this.get_pawn(i).get_color() === color_player){
                list_new_coord = this.get_moves_of_current_pawn(this.get_pawn(i));
                count_of_pawn_index++;
                for(let j = 0; j < list_new_coord.length; j++){
                    current_move = new Move(this.get_pawn(i).get_color(), this.get_pawn(i).get_coordinates(), list_new_coord[j]);
                    moves_list_player.push(current_move);
                }
            }
            if(count_of_pawn_index === count_of_pawn){
                return moves_list_player;
            }
        }

        return 'error: with color';
    }

    get_moves_of_current_pawn(current_pawn) {
        if (current_pawn != undefined) {
            let moves_list = [];
            let current_pawn_coordinates = current_pawn.get_coordinates();
            let current_pawn_color = current_pawn.get_color();

            this.add_horizontal_moves(current_pawn_coordinates, current_pawn_color, moves_list);
            this.add_vertical_moves(current_pawn_coordinates, current_pawn_color, moves_list);
            this.add_diagonal1_moves(current_pawn_coordinates, current_pawn_color, moves_list);
            this.add_diagonal2_moves(current_pawn_coordinates, current_pawn_color, moves_list);
            return moves_list;
        }
    }

    check_not_blocked_vertical_path(current_coord, potential_coord, opponent_color, side)
    {
        if(side === '+')
        {
            let move_coord = new Coordinates(current_coord.x(), current_coord.y());
            while(move_coord.x() < potential_coord.x())
            {
                if(this.find_pawn(move_coord) === opponent_color)
                    return false;
                move_coord._x++;
            }
        }
        if(side === '-')
        {
            let move_coord = new Coordinates(current_coord.x(), current_coord.y());
            while(move_coord.x() > potential_coord.x())
            {
                if(this.find_pawn(move_coord) === opponent_color)
                    return false;
                move_coord._x--;

            }
        }
        return true;
    }

    add_vertical_moves(current_pawn_coordinates, current_pawn_color, moves_list) {
        let number_pawn_aligned = 0;
        let potential_new_coord_pawn;
        let opponent_pawn_color = current_pawn_color === Color.BLACK ? Color.WHITE : Color.BLACK;

        for (let i = 0; i < Dimension.DEFAULT_WIDTH; i++) {
            let i_pawn_color = this.find_pawn(new Coordinates(i, current_pawn_coordinates.y()));
            if (i_pawn_color !== Color.NONE) {
                number_pawn_aligned++;
            }
        }

        if (current_pawn_coordinates.x() + number_pawn_aligned < Dimension.DEFAULT_WIDTH) {
            potential_new_coord_pawn = new Coordinates(current_pawn_coordinates.x() + number_pawn_aligned, current_pawn_coordinates.y());

            if(this.check_not_blocked_vertical_path(current_pawn_coordinates,
                potential_new_coord_pawn, opponent_pawn_color, '+')) {
                if (this.find_pawn(potential_new_coord_pawn) !== current_pawn_color) {
                    moves_list.push(potential_new_coord_pawn);
                }
            }
        }


        if(current_pawn_coordinates.x() - number_pawn_aligned >= 0 ) {
            potential_new_coord_pawn = new Coordinates(current_pawn_coordinates.x() - number_pawn_aligned, current_pawn_coordinates.y());

            if(this.check_not_blocked_vertical_path(current_pawn_coordinates,
                potential_new_coord_pawn, opponent_pawn_color, '-')) {
                if (this.find_pawn(potential_new_coord_pawn) !== current_pawn_color) {
                    moves_list.push(potential_new_coord_pawn);
                }
            }
        }
    }

    check_not_blocked_horizontal_path(current_coord, potential_coord, opponent_color, side)
    {
        if(side === '+')
        {
            let move_coord = new Coordinates(current_coord.x(), current_coord.y());
            while(move_coord.y() < potential_coord.y())
            {
                if(this.find_pawn(move_coord) === opponent_color)
                    return false;
                move_coord._y++;
            }
        }
        if(side === '-')
        {
            let move_coord = new Coordinates(current_coord.x(), current_coord.y());
            while(move_coord.y() > potential_coord.y())
            {
                if(this.find_pawn(move_coord) === opponent_color)
                    return false;
                move_coord._y--;

            }
        }
        return true;
    }

    add_horizontal_moves(current_pawn_coordinates, current_pawn_color, moves_list) {
        let number_pawn_aligned = 0;
        let potential_new_coord_pawn;
        let opponent_pawn_color = current_pawn_color === Color.BLACK ? Color.WHITE : Color.BLACK;

        for (let i = 0; i < Dimension.DEFAULT_HEIGHT; i++) {
            if (this.find_pawn(new Coordinates(current_pawn_coordinates.x(), i)) !== Color.NONE) {
                number_pawn_aligned++;
            }
        }

        if (current_pawn_coordinates.y() + number_pawn_aligned < Dimension.DEFAULT_HEIGHT) {
            potential_new_coord_pawn = new Coordinates(current_pawn_coordinates.x(), current_pawn_coordinates.y() + number_pawn_aligned);
            if(this.check_not_blocked_horizontal_path(current_pawn_coordinates,
                potential_new_coord_pawn, opponent_pawn_color, '+')) {
                if (this.find_pawn(potential_new_coord_pawn) !== current_pawn_color) {
                    moves_list.push(potential_new_coord_pawn);
                }
            }
        }

        if (current_pawn_coordinates.y() - number_pawn_aligned >= 0) {
            potential_new_coord_pawn = new Coordinates(current_pawn_coordinates.x(), current_pawn_coordinates.y() - number_pawn_aligned);
            if(this.check_not_blocked_horizontal_path(current_pawn_coordinates,
                potential_new_coord_pawn, opponent_pawn_color, '-')) {
                if (this.find_pawn(potential_new_coord_pawn) !== current_pawn_color) {
                    moves_list.push(potential_new_coord_pawn);
                }
            }
        }
    }

    check_not_blocked_diagonal1_path(current_coord, potential_coord, opponent_color, side)
    {
        if(side === '+')
        {
            let move_coord = new Coordinates(current_coord.x(), current_coord.y());
            while(move_coord.x() < potential_coord.x() && move_coord.y() < potential_coord.y())
            {
                if(this.find_pawn(move_coord) === opponent_color)
                    return false;
                move_coord._x++;
                move_coord._y++;
            }
        }
        if(side === '-')
        {
            let move_coord = new Coordinates(current_coord.x(), current_coord.y());
            while(move_coord.x() > potential_coord.x() && move_coord.y() > potential_coord.y())
            {
                if(this.find_pawn(move_coord) === opponent_color)
                    return false;
                move_coord._x--;
                move_coord._y--;

            }
        }
        return true;
    }

    add_diagonal1_moves(current_pawn_coordinates, current_pawn_color, moves_list) {
        let number_pawn_aligned = 0;
        let potential_new_coord_pawn;
        let opponent_pawn_color = current_pawn_color === Color.BLACK ? Color.WHITE : Color.BLACK;
        let number_case_diagonal = current_pawn_coordinates.x() >= current_pawn_coordinates.y() ? (Dimension.DEFAULT_HEIGHT - current_pawn_coordinates.x() + current_pawn_coordinates.y()) : (Dimension.DEFAULT_WIDTH - current_pawn_coordinates.y() + current_pawn_coordinates.x());

        let coord_potential_pawn = new Coordinates(current_pawn_coordinates.x(), current_pawn_coordinates.y());

        while(coord_potential_pawn.x() !== 0 && coord_potential_pawn.y() !== 0){


            coord_potential_pawn._x --;
            coord_potential_pawn._y --;
        }

        for (let i = 0; i < number_case_diagonal; i++) {
            if (this.find_pawn(coord_potential_pawn) !== Color.NONE) {
                number_pawn_aligned++;
            }
            coord_potential_pawn._x++;
            coord_potential_pawn._y++;
        }

        if ((current_pawn_coordinates.x() + number_pawn_aligned < Dimension.DEFAULT_WIDTH) && (current_pawn_coordinates.y() + number_pawn_aligned < Dimension.DEFAULT_HEIGHT)) {
            potential_new_coord_pawn = new Coordinates(current_pawn_coordinates.x() + number_pawn_aligned, current_pawn_coordinates.y() + number_pawn_aligned);
            if(this.check_not_blocked_diagonal1_path(current_pawn_coordinates,
                potential_new_coord_pawn, opponent_pawn_color, '+')) {
                if (this.find_pawn(potential_new_coord_pawn) !== current_pawn_color) {
                    moves_list.push(potential_new_coord_pawn);
                }
            }
        }

        if ((current_pawn_coordinates.x() - number_pawn_aligned >= 0) && (current_pawn_coordinates.y() - number_pawn_aligned >= 0)) {
            potential_new_coord_pawn = new Coordinates(current_pawn_coordinates.x() - number_pawn_aligned, current_pawn_coordinates.y() - number_pawn_aligned);
            if(this.check_not_blocked_diagonal1_path(current_pawn_coordinates,
                potential_new_coord_pawn, opponent_pawn_color, '-')) {
                if (this.find_pawn(potential_new_coord_pawn) !== current_pawn_color) {
                    moves_list.push(potential_new_coord_pawn);
                }
            }
        }
    }

    check_not_blocked_diagonal2_path(current_coord, potential_coord, opponent_color, side)
    {
        if(side === '+')
        {
            let move_coord = new Coordinates(current_coord.x(), current_coord.y());
            while(move_coord.x() < potential_coord.x() && move_coord.y() > potential_coord.y())
            {
                if(this.find_pawn(move_coord) === opponent_color)
                    return false;
                move_coord._x++;
                move_coord._y--;
            }
        }
        if(side === '-')
        {
            let move_coord = new Coordinates(current_coord.x(), current_coord.y());
            while(move_coord.x() > potential_coord.x() && move_coord.y() < potential_coord.y())
            {
                if(this.find_pawn(move_coord) === opponent_color)
                    return false;
                move_coord._x--;
                move_coord._y++;
            }
        }
        return true;
    }

    add_diagonal2_moves(current_pawn_coordinates, current_pawn_color, moves_list){
        let number_pawn_aligned = 0;
        let potential_new_coord_pawn;
        let opponent_pawn_color = current_pawn_color === Color.BLACK ? Color.WHITE : Color.BLACK;
        let number_case_diagonal = (current_pawn_coordinates.x() + current_pawn_coordinates.y() + 1) < Dimension.DEFAULT_WIDTH ? (current_pawn_coordinates.x() + current_pawn_coordinates.y() + 1) : (Dimension.DEFAULT_WIDTH * 2 - 1 - current_pawn_coordinates.x() - current_pawn_coordinates.y());
        let coord_potential_pawn = new Coordinates(current_pawn_coordinates.x(), current_pawn_coordinates.y());

        while (coord_potential_pawn.x() !== 7 && coord_potential_pawn.y() !== 0) {
            coord_potential_pawn._x++;
            coord_potential_pawn._y--;
        }
        for (let i = 0; i < number_case_diagonal; i++) {
            if (this.find_pawn(coord_potential_pawn) !== Color.NONE) {
                number_pawn_aligned++;
            }
            coord_potential_pawn._x--;
            coord_potential_pawn._y++;
        }
        if ((current_pawn_coordinates.x() + number_pawn_aligned < Dimension.DEFAULT_WIDTH) && (current_pawn_coordinates.y() - number_pawn_aligned >= 0)) {
            potential_new_coord_pawn = new Coordinates(current_pawn_coordinates.x() + number_pawn_aligned, current_pawn_coordinates.y() - number_pawn_aligned);
            if(this.check_not_blocked_diagonal2_path(current_pawn_coordinates,
                potential_new_coord_pawn, opponent_pawn_color, '+')) {
                if (this.find_pawn(potential_new_coord_pawn) !== current_pawn_color) {
                    moves_list.push(potential_new_coord_pawn);
                }
            }
        }
        if ((current_pawn_coordinates.x() - number_pawn_aligned >= 0) && (current_pawn_coordinates.y() + number_pawn_aligned < Dimension.DEFAULT_HEIGHT)) {
            potential_new_coord_pawn = new Coordinates(current_pawn_coordinates.x() - number_pawn_aligned, current_pawn_coordinates.y() + number_pawn_aligned);
            if(this.check_not_blocked_diagonal2_path(current_pawn_coordinates,
                potential_new_coord_pawn, opponent_pawn_color, '-')) {
                if (this.find_pawn(potential_new_coord_pawn) !== current_pawn_color) {
                    moves_list.push(potential_new_coord_pawn);
                }
            }
        }
    }

}

export default PawnLists;
