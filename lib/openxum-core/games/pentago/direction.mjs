"use strict";

let Direction = {CLOCKWISE: 0, ANTI_CLOCKWISE: 1};

export default Direction;