require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const OpenXum = require('../../../lib/openxum-core/').default;
const AI = require('../../../lib/openxum-ai/').default;

let e = new OpenXum.Kikotsoka.Engine(OpenXum.Kikotsoka.GameType.SMALL, OpenXum.Kikotsoka.Color.BLACK);
let p1 = new AI.Specific.Kikotsoka.RandomPlayer(OpenXum.Kikotsoka.Color.BLACK, OpenXum.Kikotsoka.Color.WHITE, e);
//let p1 = new AI.Specific.Kikotsoka.MCTSPlayer(OpenXum.Kikotsoka.Color.BLACK, OpenXum.Kikotsoka.Color.WHITE, e);
//let p2 = new AI.Generic.RandomPlayer(OpenXum.Kikotsoka.Color.WHITE, OpenXum.Kikotsoka.Color.BLACK, e);
let p2 = new AI.Specific.Kikotsoka.MCTSPlayer(OpenXum.Kikotsoka.Color.WHITE, OpenXum.Kikotsoka.Color.BLACK, e);
let p = p1;
let moves = [];
let index = 0;
let black_level = e._black_level;
let white_level = e._white_level;

while (!e.is_finished()) {
  ++index;
//  console.log("Move n°" + index);
//  console.log("Move number: " + e.get_possible_move_list().length);

  let move = p.move();

  moves.push(move);
  e.move(move);

  console.log(move.to_string());
//  console.log(e.to_string());

  p = p === p1 ? p2 : p1;

/*  if (black_level !== e._black_level) {
    black_level = e._black_level;
    console.log("NEXT LEVEL: black => " + black_level);
  }
  if (white_level !== e._white_level) {
    white_level = e._white_level;
    console.log("NEXT LEVEL: white => " + white_level);
  } */
}

console.log("Winner is " +
  (e.winner_is() === OpenXum.Kikotsoka.Color.BLACK ? "black" :
    (e.winner_is() === OpenXum.Kikotsoka.Color.WHITE ? "white" : "nobody")));
console.log("Move number = " + moves.length);
console.log("Black level = " + e._black_level);
console.log("White level = " + e._white_level);
console.log("Black captured pieces = " + e._black_captured_piece_number);
console.log("White captured pieces = " + e._white_captured_piece_number);

let str = "";

for (let index = 0; index < moves.length; ++index) {
  str += moves[index].encode() + ';';
}
console.log(str);