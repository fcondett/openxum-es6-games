require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const OpenXum = require('../../../lib/openxum-core/').default;
const AI = require('../../../lib/openxum-ai/').default;

let e;
let p1;
let p2;
let p;
let moves;

for(let i = 0; i < 10; i++) {
    e = new OpenXum.Lines_of_action.Engine(OpenXum.Lines_of_action.GameType.STANDARD, OpenXum.Lines_of_action.Color.WHITE);
    p1 = new AI.Specific.Lines_of_action.IALinesOfActionPlayer(OpenXum.Lines_of_action.Color.WHITE, OpenXum.Lines_of_action.Color.BLACK, e);
    p2 = new AI.Generic.RandomPlayer(OpenXum.Lines_of_action.Color.BLACK, OpenXum.Lines_of_action.Color.WHITE, e);
    p = p1;
    moves = [];

    while (!e.is_finished()) {
        const move = p.move();

        moves.push(move);
        e.move(move);

        p = p === p1 ? p2 : p1;
    }
}