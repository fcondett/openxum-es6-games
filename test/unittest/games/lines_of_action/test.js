require = require('@std/esm')(module, { esm: 'mjs', cjs: true });

const OpenXum = require('../../../../lib/openxum-core/').default;

describe('Coordinates', () => {
    test('to string', () => {
        const coordinates = new OpenXum.Lines_of_action.Coordinates(0,0);
        const str = coordinates.to_string();

        expect(str).toEqual(expect.any(String));
        expect(str).toBe('(0,0)');
    });
});

describe('Engine', () => {
    test('get name', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.BLACK;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        expect(engine.get_name()).toEqual(expect.any(String));
        expect(engine.get_name()).toBe('Lines_of_action');
    })

    test('board size', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.BLACK;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);
        const width = engine._board.getWidth();
        const height = engine._board.getHeight();

        expect(width).toBe(8);
        expect(height).toBe(8);
    })

    test('current color', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.BLACK;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);
        expect(engine.current_color()).toEqual(OpenXum.Lines_of_action.Color.BLACK);

    })

    test('change color', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.BLACK;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);
        engine._change_color();
        expect(engine.current_color()).toEqual(OpenXum.Lines_of_action.Color.WHITE);
        engine._change_color();
        expect(engine.current_color()).toEqual(OpenXum.Lines_of_action.Color.BLACK);

    })

})

describe('Board', () => {
    test('initialize board', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        expect(engine.get_cell_at(0, 1)._color).toEqual(OpenXum.Lines_of_action.Color.BLACK);
        expect(engine.get_cell_at(0, 2)._color).toEqual(OpenXum.Lines_of_action.Color.BLACK);
        expect(engine.get_cell_at(3, 0)._color).toEqual(OpenXum.Lines_of_action.Color.WHITE);
        expect(engine.get_cell_at(2, 2)._color).toEqual(OpenXum.Lines_of_action.Color.NONE);
    })

    test('move Pawn on empty space', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;
        const coord = new OpenXum.Lines_of_action.Coordinates(2,2);

        const engine = new OpenXum.Lines_of_action.Engine(t, c);
        const Pawn = engine.get_cell_at(1,0);

        engine._board.move_pawn(new OpenXum.Lines_of_action.Move(c, coord, new OpenXum.Lines_of_action.Coordinates(2,2)));
        expect(engine.get_cell_at(2, 2)._color).toEqual(OpenXum.Lines_of_action.Color.WHITE);
    })

    test('move Pawn on own space', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;
        const coord = new OpenXum.Lines_of_action.Coordinates(1,0);

        const engine = new OpenXum.Lines_of_action.Engine(t, c);
        const Pawn = engine.get_cell_at(1,0);

        engine._board.move_pawn(new OpenXum.Lines_of_action.Move(c, coord, new OpenXum.Lines_of_action.Coordinates(2,0)));
        expect(engine.get_cell_at(2, 0)._color).toEqual(OpenXum.Lines_of_action.Color.WHITE);
    })

    test('move Pawn on opponent space', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;
        const coord = new OpenXum.Lines_of_action.Coordinates(1,0);

        const engine = new OpenXum.Lines_of_action.Engine(t, c);
        const Pawn = engine.get_cell_at(1,0);

        engine._board.move_pawn(new OpenXum.Lines_of_action.Move(c, coord, new OpenXum.Lines_of_action.Coordinates(0,1)));
        expect(engine.get_cell_at(0, 1)._color).toEqual(OpenXum.Lines_of_action.Color.WHITE);
    })
})

describe('Pawn', () => {
    test('to string', () => {
        const c = OpenXum.Lines_of_action.Color.WHITE;
        const coord = new OpenXum.Lines_of_action.Coordinates(1,0);
        const Pawn = new OpenXum.Lines_of_action.Pawn(c, coord);
        const str = Pawn.to_string();

        expect(str).toEqual('[color: 2 coord: (1,0)]');
    })

})

describe('Pawn Lists', () => {
    test('constructor', () => {
        const pawnlist = new OpenXum.Lines_of_action.PawnLists();

        expect(pawnlist.nb_pawn()).toEqual(24);
        expect(pawnlist.count_black_pawn()).toEqual(12);
        expect(pawnlist.count_white_pawn()).toEqual(12);
    })


    test('find pawn', () => {
        const pawnlist = new OpenXum.Lines_of_action.PawnLists();
        const c = OpenXum.Lines_of_action.Color.WHITE;
        const coord = new OpenXum.Lines_of_action.Coordinates(5,7);
        const pawn = new OpenXum.Lines_of_action.Pawn(c, coord);

        expect(pawnlist.find_pawn(new OpenXum.Lines_of_action.Coordinates(5, 7))).toEqual(c);
    })

    test('delete pawn', () => {
        const pawnlist = new OpenXum.Lines_of_action.PawnLists();
        const c = OpenXum.Lines_of_action.Color.WHITE;
        const coord1 = new OpenXum.Lines_of_action.Coordinates(7,2);

        pawnlist.delete_pawn(7,2);
        expect(pawnlist.find_pawn(new OpenXum.Lines_of_action.Coordinates((7,2))))
            .toEqual(OpenXum.Lines_of_action.Color.NONE);
    })
})

describe('Move', () => {
    test('to string', () => {
        const c = OpenXum.Lines_of_action.Color.WHITE;
        const from = new OpenXum.Lines_of_action.Coordinates(1,0);
        const to = new OpenXum.Lines_of_action.Coordinates(1,5);
        const move = new OpenXum.Lines_of_action.Move(c, from, to);
        const str = move.to_string();

        expect(str).toEqual('white move from:(1,0) to:(1,5) ;');
    })
})

describe('Possible horizontal moves', () => {
    test('horizontal moves of B0,1 at start', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        let moves_list = [];
        engine._board._pawnList.add_horizontal_moves(new OpenXum.Lines_of_action.Coordinates(0, 1),
            OpenXum.Lines_of_action.Color.BLACK, moves_list);

        expect(moves_list).toEqual([new OpenXum.Lines_of_action.Coordinates(0, 7)]);
    })

    test('horizontal moves of W3,0 at start', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        let moves_list = [];
        engine._board._pawnList.add_horizontal_moves(new OpenXum.Lines_of_action.Coordinates(3, 0),
            OpenXum.Lines_of_action.Color.WHITE, moves_list);

        expect(moves_list).toEqual([new OpenXum.Lines_of_action.Coordinates(3, 2)]);
    })

    test('horizontal moves of B0,2 at start', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        let moves_list = [];
        engine._board._pawnList.add_horizontal_moves(new OpenXum.Lines_of_action.Coordinates(0, 2),
            OpenXum.Lines_of_action.Color.BLACK, moves_list);

        expect(moves_list).toEqual([]);
    })

    test('horizontal moves of W3,7 at start', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        let moves_list = [];
        engine._board._pawnList.add_horizontal_moves(new OpenXum.Lines_of_action.Coordinates(4, 7),
            OpenXum.Lines_of_action.Color.WHITE, moves_list);

        expect(moves_list).toEqual([new OpenXum.Lines_of_action.Coordinates(4, 5)]);
    })
})

describe('Possible vertical moves', () => {
    test('vertical moves of B0,1 at start', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        let moves_list = [];
        engine._board._pawnList.add_vertical_moves(new OpenXum.Lines_of_action.Coordinates(0, 1),
            OpenXum.Lines_of_action.Color.BLACK, moves_list);

        expect(moves_list).toEqual([new OpenXum.Lines_of_action.Coordinates(2, 1)]);
    })

    test('vertical moves of W3,0 at start', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        let moves_list = [];
        engine._board._pawnList.add_vertical_moves(new OpenXum.Lines_of_action.Coordinates(3, 0),
            OpenXum.Lines_of_action.Color.WHITE, moves_list);

        expect(moves_list).toEqual([]);
    })

    test('vertical moves of B0,2 at start', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        let moves_list = [];
        engine._board._pawnList.add_vertical_moves(new OpenXum.Lines_of_action.Coordinates(0, 2),
            OpenXum.Lines_of_action.Color.BLACK, moves_list);

        expect(moves_list).toEqual([new OpenXum.Lines_of_action.Coordinates(2, 2)]);
    })

    test('vertical moves of W3,7 at start', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        let moves_list = [];
        engine._board._pawnList.add_vertical_moves(new OpenXum.Lines_of_action.Coordinates(4, 7),
            OpenXum.Lines_of_action.Color.WHITE, moves_list);

        expect(moves_list).toEqual([]);
    })

    test('vertical moves of B7,5 at start', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        let moves_list = [];
        engine._board._pawnList.add_vertical_moves(new OpenXum.Lines_of_action.Coordinates(7, 5),
            OpenXum.Lines_of_action.Color.BLACK, moves_list);

        expect(moves_list).toEqual([new OpenXum.Lines_of_action.Coordinates(5, 5)]);
    })
})

describe('Possible moves list at start for a single pawn', () => {
    test('moves of B0,1 at start', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);
        const pawn = new OpenXum.Lines_of_action.Pawn(OpenXum.Lines_of_action.Color.BLACK,
            new OpenXum.Lines_of_action.Coordinates(0,1));

        let moves_list = [];
        moves_list = engine._board.get_possible_moves_of(pawn);

        const expected = [new OpenXum.Lines_of_action.Coordinates(0,7),
            new OpenXum.Lines_of_action.Coordinates(2,1),
            new OpenXum.Lines_of_action.Coordinates(2, 3)];

        expect(moves_list).toEqual(expected);
    })

    test('moves of B0,2 at start', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);
        const pawn = new OpenXum.Lines_of_action.Pawn(OpenXum.Lines_of_action.Color.BLACK,
            new OpenXum.Lines_of_action.Coordinates(0,2));

        let moves_list = [];
        moves_list = engine._board.get_possible_moves_of(pawn);

        const expected = [new OpenXum.Lines_of_action.Coordinates(2,2),
            new OpenXum.Lines_of_action.Coordinates(2, 4),
            new OpenXum.Lines_of_action.Coordinates(2, 0)];

        expect(moves_list).toEqual(expected);
    })

    test('moves of W3,0 at start', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);
        const pawn = new OpenXum.Lines_of_action.Pawn(OpenXum.Lines_of_action.Color.WHITE,
            new OpenXum.Lines_of_action.Coordinates(3,0));

        let moves_list = engine._board.get_possible_moves_of(pawn);

        const expected = [new OpenXum.Lines_of_action.Coordinates(3,2),
            new OpenXum.Lines_of_action.Coordinates(5, 2), new OpenXum.Lines_of_action.Coordinates(1, 2)];

        expect(moves_list).toEqual(expected);
    })

    test('moves of B7,6 at start', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);
        const pawn = new OpenXum.Lines_of_action.Pawn(OpenXum.Lines_of_action.Color.BLACK,
            new OpenXum.Lines_of_action.Coordinates(7,6));

        let moves_list = engine._board.get_possible_moves_of(pawn);

        const expected = [new OpenXum.Lines_of_action.Coordinates(7,0),
            new OpenXum.Lines_of_action.Coordinates(5,6),
            new OpenXum.Lines_of_action.Coordinates(5,4)];

        expect(moves_list).toEqual(expected);
    })

    test('moves of W5,7 at start', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);
        const pawn = new OpenXum.Lines_of_action.Pawn(OpenXum.Lines_of_action.Color.WHITE,
            new OpenXum.Lines_of_action.Coordinates(5,7));

        let moves_list = engine._board.get_possible_moves_of(pawn);

        const expected = [new OpenXum.Lines_of_action.Coordinates(5,5),
            new OpenXum.Lines_of_action.Coordinates(3,5),
            new OpenXum.Lines_of_action.Coordinates(7,5)];

        expect(moves_list).toEqual(expected);
    })
})

describe('Possible moves for white after white turn', () => {
    test('moves for W1,2 after W1,0', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        const move = new OpenXum.Lines_of_action.Move
        (c, new OpenXum.Lines_of_action.Coordinates(1,0),new OpenXum.Lines_of_action.Coordinates(1,2));

        engine.move(move);

        expect(engine._board._pawnList.find_pawn(new OpenXum.Lines_of_action.Coordinates(1,2)))
            .toEqual(OpenXum.Lines_of_action.Color.WHITE);

        let moves_list = engine.get_possible_moves_of(new OpenXum.Lines_of_action.Pawn
        (OpenXum.Lines_of_action.Color.WHITE, new OpenXum.Lines_of_action.Coordinates(1,2)));

        const expected = [ new OpenXum.Lines_of_action.Coordinates(1,4), new OpenXum.Lines_of_action.Coordinates(1,0),
            new OpenXum.Lines_of_action.Coordinates(4,2), new OpenXum.Lines_of_action.Coordinates(4,5)];

        expect(moves_list).toEqual(expected);

    })

    test('moves for W4,2 after W1,2', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        const move = new OpenXum.Lines_of_action.Move
        (c, new OpenXum.Lines_of_action.Coordinates(1,0),new OpenXum.Lines_of_action.Coordinates(1,2));

        engine.move(move);

        const move2 = new OpenXum.Lines_of_action.Move
        (c, new OpenXum.Lines_of_action.Coordinates(1,2),new OpenXum.Lines_of_action.Coordinates(4,2));

        engine.move(move2);

        let moves_list = engine.get_possible_moves_of(new OpenXum.Lines_of_action.Pawn
        (OpenXum.Lines_of_action.Color.WHITE, new OpenXum.Lines_of_action.Coordinates(4,2)));

        const expected = [new OpenXum.Lines_of_action.Coordinates(4,5),
            new OpenXum.Lines_of_action.Coordinates(7,2),
            new OpenXum.Lines_of_action.Coordinates(1,2),
            new OpenXum.Lines_of_action.Coordinates(7,5),
            new OpenXum.Lines_of_action.Coordinates(1,5)];

        expect(moves_list).toEqual(expected);

    })

})

/*describe('Possible moves list for turn 1', () => {
    test('moves for white for turn 1', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        let moves_list = engine._board.get_possible_moves_of_player(c);

        const expected = [new OpenXum.Lines_of_action.Coordinates(1,2),
            new OpenXum.Lines_of_action.Coordinates(7,0),
            new OpenXum.Lines_of_action.Coordinates(3,2),

            new OpenXum.Lines_of_action.Coordinates(2,2),
            new OpenXum.Lines_of_action.Coordinates(4,2),
            new OpenXum.Lines_of_action.Coordinates(0,2),

            new OpenXum.Lines_of_action.Coordinates(3,2),
            new OpenXum.Lines_of_action.Coordinates(5,2),
            new OpenXum.Lines_of_action.Coordinates(1,2),

            new OpenXum.Lines_of_action.Coordinates(4,2),
            new OpenXum.Lines_of_action.Coordinates(6,2),
            new OpenXum.Lines_of_action.Coordinates(2,2),

            new OpenXum.Lines_of_action.Coordinates(5,2),
            new OpenXum.Lines_of_action.Coordinates(7,2),
            new OpenXum.Lines_of_action.Coordinates(3,2),

            new OpenXum.Lines_of_action.Coordinates(6,2),
            new OpenXum.Lines_of_action.Coordinates(0,0),
            new OpenXum.Lines_of_action.Coordinates(4,2),

            new OpenXum.Lines_of_action.Coordinates(1,5),
            new OpenXum.Lines_of_action.Coordinates(7,7),
            new OpenXum.Lines_of_action.Coordinates(3,5),

            new OpenXum.Lines_of_action.Coordinates(2,5),
            new OpenXum.Lines_of_action.Coordinates(0,5),
            new OpenXum.Lines_of_action.Coordinates(4,5),

            new OpenXum.Lines_of_action.Coordinates(3,5),
            new OpenXum.Lines_of_action.Coordinates(1,5),
            new OpenXum.Lines_of_action.Coordinates(5,5),

            new OpenXum.Lines_of_action.Coordinates(4,5),
            new OpenXum.Lines_of_action.Coordinates(2,5),
            new OpenXum.Lines_of_action.Coordinates(6,5),

            new OpenXum.Lines_of_action.Coordinates(5,5),
            new OpenXum.Lines_of_action.Coordinates(3,5),
            new OpenXum.Lines_of_action.Coordinates(7,5),

            new OpenXum.Lines_of_action.Coordinates(6,5),
            new OpenXum.Lines_of_action.Coordinates(0,7),
            new OpenXum.Lines_of_action.Coordinates(4,5)];

        expect(moves_list).toEqual(expected);
    })
})*/

describe('Possible moves with conflicts', () => {
    test('moves for B0,2 after W2,0', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        const move = new OpenXum.Lines_of_action.Move
        (c, new OpenXum.Lines_of_action.Coordinates(2,0),new OpenXum.Lines_of_action.Coordinates(2,2));

        engine.move(move);

        let moves_list = engine._board.get_possible_moves_of(
            new OpenXum.Lines_of_action.Pawn(OpenXum.Lines_of_action.Color.BLACK,
                new OpenXum.Lines_of_action.Coordinates(0,2)));

        const expected = [new OpenXum.Lines_of_action.Coordinates(2,4), new OpenXum.Lines_of_action.Coordinates(1,1)];

        expect(moves_list).toEqual(expected);

    })

    test('moves for W2,0 after B0,2', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.BLACK;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        const move = new OpenXum.Lines_of_action.Move
        (c, new OpenXum.Lines_of_action.Coordinates(0,2),new OpenXum.Lines_of_action.Coordinates(2,2));

        engine.move(move);

        let moves_list = engine._board.get_possible_moves_of(
            new OpenXum.Lines_of_action.Pawn(OpenXum.Lines_of_action.Color.WHITE,
                new OpenXum.Lines_of_action.Coordinates(2,0)));

        const expected = [new OpenXum.Lines_of_action.Coordinates(4,2), new OpenXum.Lines_of_action.Coordinates(1,1)];

        expect(moves_list).toEqual(expected);

    })

    test('moves for B7,2 after W6,0', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        const move = new OpenXum.Lines_of_action.Move
        (c, new OpenXum.Lines_of_action.Coordinates(6,0),new OpenXum.Lines_of_action.Coordinates(6,2));

        engine.move(move);

        let moves_list = engine._board.get_possible_moves_of(
            new OpenXum.Lines_of_action.Pawn(OpenXum.Lines_of_action.Color.BLACK,
                new OpenXum.Lines_of_action.Coordinates(7,2)));

        const expected = [new OpenXum.Lines_of_action.Coordinates(5,0),
            new OpenXum.Lines_of_action.Coordinates(5,4)];

        expect(moves_list).toEqual(expected);

    })

    test('moves for B7,3 after W6,0', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        const move = new OpenXum.Lines_of_action.Move
        (c, new OpenXum.Lines_of_action.Coordinates(6,0),new OpenXum.Lines_of_action.Coordinates(6,2));

        engine.move(move);

        let moves_list = engine._board.get_possible_moves_of(
            new OpenXum.Lines_of_action.Pawn(OpenXum.Lines_of_action.Color.BLACK,
                new OpenXum.Lines_of_action.Coordinates(7,3)));

        const expected = [new OpenXum.Lines_of_action.Coordinates(5,3),
            new OpenXum.Lines_of_action.Coordinates(5,5)];

        expect(moves_list).toEqual(expected);

    })

    test('moves for B2,0 after B0,2', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.BLACK;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        const move = new OpenXum.Lines_of_action.Move
        (c, new OpenXum.Lines_of_action.Coordinates(0,2),new OpenXum.Lines_of_action.Coordinates(2,0));

        engine.move(move);

        let moves_list = engine._board.get_possible_moves_of(
            new OpenXum.Lines_of_action.Pawn(OpenXum.Lines_of_action.Color.BLACK,
                new OpenXum.Lines_of_action.Coordinates(2,0)));

        const expected = [new OpenXum.Lines_of_action.Coordinates(2,2),
            new OpenXum.Lines_of_action.Coordinates(4,2), new OpenXum.Lines_of_action.Coordinates(1,1)];

        expect(moves_list).toEqual(expected);

    })
})

describe('pawns connected class', () =>{
    test('get neightbors of W2,0', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);
        const pawn10 = engine.get_cell_at(1,0);
        const pawn20 = engine.get_cell_at(2,0);
        const pawn30 = engine.get_cell_at(3,0);


        const pawns_connected = new OpenXum.Lines_of_action.ConnectedPawns(pawn20,
            engine._board._pawnList.count_white_pawn(), engine._board._board);

        pawns_connected._get_neighbors_of(pawn20);

        expect(pawns_connected._neighbors).toEqual([pawn10,pawn30]);

    })

    test('get neighbors of W1,0', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);
        const pawn10 = engine.get_cell_at(1,0);
        const pawn20 = engine.get_cell_at(2,0);
        const pawn30 = engine.get_cell_at(3,0);


        const pawns_connected = new OpenXum.Lines_of_action.ConnectedPawns(pawn10,
            engine._board._pawnList.count_white_pawn(), engine._board._board);

        pawns_connected._get_neighbors_of(pawn10);

        expect(pawns_connected._neighbors).toEqual([pawn20]);

    })

})

describe('get winner', () => {
    test('get winner at start', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        expect(engine.is_finished()).toEqual(false);

        expect(engine.winner_is()).toEqual(OpenXum.Lines_of_action.Color.NONE);
    })

    test('get winner if there is only 1 pawn left', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(2,0));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(3,0));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(4,0));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(5,0));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(6,0));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(1,7));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(2,7));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(3,7));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(4,7));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(5,7));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(6,7));

        expect(engine._board._pawnList.count_white_pawn()).toEqual(1);

        expect(engine.is_finished()).toEqual(true);

        expect(engine.winner_is()).toEqual(OpenXum.Lines_of_action.Color.WHITE);
    })

    test('get winner if there are 2 pawns left', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(3,0));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(4,0));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(5,0));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(6,0));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(1,7));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(2,7));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(3,7));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(4,7));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(5,7));
        engine._board.delete_pawn(new OpenXum.Lines_of_action.Coordinates(6,7));

        expect(engine._board._pawnList.count_white_pawn()).toEqual(2);

        expect(engine.is_finished()).toEqual(true);

        expect(engine.winner_is()).toEqual(OpenXum.Lines_of_action.Color.WHITE);
    })

    test('get winner with 4 pawns and a simple shape', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        const move1 = new OpenXum.Lines_of_action.Move(c,new OpenXum.Lines_of_action.Coordinates(1,0),
            new OpenXum.Lines_of_action.Coordinates(1,2));
        engine.move(move1);

        const move2 = new OpenXum.Lines_of_action.Move(c,new OpenXum.Lines_of_action.Coordinates(2,0),
            new OpenXum.Lines_of_action.Coordinates(2,1));
        engine.move(move2);

        const move3 = new OpenXum.Lines_of_action.Move(c,new OpenXum.Lines_of_action.Coordinates(3,0),
            new OpenXum.Lines_of_action.Coordinates(3,1));
        engine.move(move3);

        const move4 = new OpenXum.Lines_of_action.Move(c,new OpenXum.Lines_of_action.Coordinates(4,0),
            new OpenXum.Lines_of_action.Coordinates(2,3));
        engine.move(move4);


        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(5,0));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(6,0));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(1,7));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(2,7));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(3,7));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(4,7));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(5,7));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(6,7));

        expect(engine.is_finished()).toEqual(true);

        expect(engine.winner_is()).toEqual(OpenXum.Lines_of_action.Color.WHITE);
    })

    test('get winner with 5 pawns and a branched path', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        const move1 = new OpenXum.Lines_of_action.Move(c,new OpenXum.Lines_of_action.Coordinates(1,0),
            new OpenXum.Lines_of_action.Coordinates(1,2));
        engine.move(move1);

        const move2 = new OpenXum.Lines_of_action.Move(c,new OpenXum.Lines_of_action.Coordinates(2,0),
            new OpenXum.Lines_of_action.Coordinates(2,1));
        engine.move(move2);

        const move3 = new OpenXum.Lines_of_action.Move(c,new OpenXum.Lines_of_action.Coordinates(3,0),
            new OpenXum.Lines_of_action.Coordinates(2,0));
        engine.move(move3);

        const move4 = new OpenXum.Lines_of_action.Move(c,new OpenXum.Lines_of_action.Coordinates(4,0),
            new OpenXum.Lines_of_action.Coordinates(3,1));
        engine.move(move4);

        const move5 = new OpenXum.Lines_of_action.Move(c,new OpenXum.Lines_of_action.Coordinates(5,0),
            new OpenXum.Lines_of_action.Coordinates(4,0));
        engine.move(move5);

        engine._change_color();

        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(6,0));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(1,7));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(2,7));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(3,7));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(4,7));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(5,7));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(6,7));

        expect(engine.is_finished()).toEqual(true);

        expect(engine.winner_is()).toEqual(OpenXum.Lines_of_action.Color.WHITE);
    })

    test('get winner with 4 pawns and a single not connected one', () => {
        const t = OpenXum.Lines_of_action.GameType.BASIC;
        const c = OpenXum.Lines_of_action.Color.WHITE;

        const engine = new OpenXum.Lines_of_action.Engine(t, c);

        const move1 = new OpenXum.Lines_of_action.Move(c,new OpenXum.Lines_of_action.Coordinates(1,0),
            new OpenXum.Lines_of_action.Coordinates(1,2));
        engine.move(move1);

        const move2 = new OpenXum.Lines_of_action.Move(c,new OpenXum.Lines_of_action.Coordinates(2,0),
            new OpenXum.Lines_of_action.Coordinates(2,1));
        engine.move(move2);

        const move3 = new OpenXum.Lines_of_action.Move(c,new OpenXum.Lines_of_action.Coordinates(3,0),
            new OpenXum.Lines_of_action.Coordinates(2,0));
        engine.move(move3);

        const move4 = new OpenXum.Lines_of_action.Move(c,new OpenXum.Lines_of_action.Coordinates(4,0),
            new OpenXum.Lines_of_action.Coordinates(3,1));
        engine.move(move4);

        const move5 = new OpenXum.Lines_of_action.Move(c,new OpenXum.Lines_of_action.Coordinates(5,0),
            new OpenXum.Lines_of_action.Coordinates(4,7));
        engine.move(move5);

        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(6,0));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(1,7));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(2,7));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(3,7));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(4,7));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(5,7));
        engine._board._pawnList.delete_pawn(new OpenXum.Lines_of_action.Coordinates(6,7));

        expect(engine.is_finished()).toEqual(false);

        expect(engine.winner_is()).toEqual(OpenXum.Lines_of_action.Color.NONE);
    })
})